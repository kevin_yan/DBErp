<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Admin\Service;

use Admin\Entity\PrintTemplate;
use Doctrine\ORM\EntityManager;

class PrintTemplateManager
{
    private $entityManager;

    public function __construct(
        EntityManager $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    /**
     * 编辑打印模板
     * @param array $data
     * @param PrintTemplate $printTemplate
     * @return void
     * @throws \Doctrine\ORM\Exception\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editPrintTemplate(array $data, PrintTemplate $printTemplate)
    {
        $printTemplate->valuesSet($data);
        $this->entityManager->flush();
    }
}